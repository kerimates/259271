#include<iostream>
#include<fstream>
#include<string.h>
#include<conio.h>
#include<iomanip>
#include<dos.h>
#include<sstream>
#include<algorithm>
#include <cctype>

using namespace std;


// ************************************** YARDIMCI FONKSIYONLAR ******************************************************************************* //



bool contains_word(const string& sentence, const string& word)
{
	size_t pos = 0;
	while ((pos = sentence.substr(pos).find(word)) != string::npos) {
		if(pos==0 && (pos+word.size())>sentence.size())return true;

		if(pos==0){
			if ( !(isalpha(sentence[pos + word.size()])) )
				return true; 
			else 
				return false;
		}

		if((pos+word.size())>sentence.size() ){
			if( !(isalpha(sentence[pos-1])) ) return true;
			else 
				return false;
		}

		if (!(isalpha(sentence[pos-1])) && !(isalpha(sentence[pos + word.size()])))   return true;
	}
	return false;
}

void myUpperCase(char *arr ){
for( int i=0 ; i < 250 ; ++i ) arr[i] = toupper( arr[i] ) ;
}

void ekraniTemizle(){
	system("cls"); 
}


// ************************************** YAZAR SINIFI ******************************************************************************* //

class Yazar
{
public:	
	char yazarAdi[250];
	char dogumYili[100];
	char dogumYeri[250];
	int popularite;

	void yazarOlusturma()
	{ 		
		cout<<"Adi Soyadi    : ";
		cin.ignore();
		cin.getline(yazarAdi,250);
		myUpperCase(yazarAdi);
		
		string line;
		while((cout << "Dogum Yili    : ") && getline(cin, line)	&& !(istringstream(line) >> dogumYili))
		{
			std::cerr << "Gecersiz Deger, Lutfen Sayi Giriniz!" << std::endl;
		}
		

		cout<<"Dogum Yeri    : ";		
		cin.getline(dogumYeri,250);
		myUpperCase(dogumYeri);
		
		while((cout << "Popularitesi  : ") && getline(cin, line)	&& !(istringstream(line) >> popularite))
		{
			std::cerr << "Gecersiz Deger, Lutfen Sayi Giriniz!" << std::endl;
		}
		cout<<endl;
	};

};


class YazarDosyasi:public Yazar
{
public:	
	void yazariOlustur();
};
void YazarDosyasi::yazariOlustur()
{
	char ad[250];

	yazarOlusturma();
	strcpy(ad,yazarAdi);
	strcat(ad,".txt");

	ofstream yazariOlustur(ad);
	yazariOlustur<<"Adi Soyadi    : "<<yazarAdi<<endl;
	yazariOlustur<<"Dogum Yili    : "<<dogumYili<<endl;
	yazariOlustur<<"Dogum Yeri    : "<<dogumYeri<<endl;
	yazariOlustur<<"Popularitesi  : "<<popularite<<endl;
	yazariOlustur.close();
}

class Yazarlar:public YazarDosyasi
{
public:
	void yazariDosyayaYaz()
	{
		fstream streamYazar("Yazarlar.txt",ios::out|ios::app);
		streamYazar.width(20);
		streamYazar<<setiosflags(ios::left);
		streamYazar<<yazarAdi;

		streamYazar.width(15);
		streamYazar<<setiosflags(ios::left);
		streamYazar<<dogumYeri;

		streamYazar.width(12);
		streamYazar<<setiosflags(ios::right);
		streamYazar<<dogumYili;

		streamYazar.width(20);
		streamYazar<<setiosflags(ios::right);
		streamYazar<<popularite<<endl;

		streamYazar.close();
	}
};                                             


class YazarKaydetme
{
public:
	YazarKaydetme();
};

YazarKaydetme::YazarKaydetme()
{
	Yazarlar yazar;
	int tercih;

	while(1)
	{
		ekraniTemizle();
		cout<<"\n\n * * * * * * * * * * * * YAZAR KAYDET * * * * * * * * * * * * ";
		cout<<"\n1.Yeni Yazar \n2.Geri \n";
		cout<<"\nLutfen Tercihinizi Giriniz:";
		cin>>tercih;

		if(tercih==1)
		{
			yazar.yazariOlustur();
			yazar.yazariDosyayaYaz();
		}else 
			break;
	}
}

// ************************************** KITAP SINIFI ******************************************************************************* //
class kitap
{
public:
	char id[100];
	char adi[250];
	char yazari[250];
	float fiati;
	int adedi;

	void olustur()
	{ 		
		cout<<"Kitap Adi   : ";
		cin.ignore();
		cin.getline(adi,250);
		myUpperCase(adi);
		

		string line;
		while((cout << "Kitap ID    : ") && getline(cin, line)	&& !(istringstream(line) >> id))
		{
			std::cerr << "Gecersiz Deger, Lutfen Sayi Giriniz!" << std::endl;
		}

		cout<<"Yazari      : ";		
		cin.getline(yazari,250);
		myUpperCase(yazari);

		string line2;
		while((cout << "Fiati       : ") && getline(cin, line2)	&& !(istringstream(line2) >> fiati))
		{
			std::cerr << "Gecersiz Deger, Lutfen Sayi Giriniz!" << std::endl;
		}
		
		string line3;
		while((cout << "Adeti       : ") && getline(cin, line3)	&& !(istringstream(line3) >> adedi))
		{
			std::cerr << "Gecersiz Deger, Lutfen Sayi Giriniz!" << std::endl;
		}

		cout<<endl;
	};

};


class dosya:public kitap
{
public:
	void hepsiListesineYaz()
	{
		fstream kitabiOlustur("Kitaplar.txt",ios::out|ios::app);
		kitabiOlustur.width(20);
		kitabiOlustur<<setiosflags(ios::left);
		kitabiOlustur<<adi;

		kitabiOlustur.width(18);
		kitabiOlustur<<setiosflags(ios::left);
		kitabiOlustur<<yazari;

		kitabiOlustur.width(9);
		kitabiOlustur<<setiosflags(ios::right);
		kitabiOlustur<<id;

		kitabiOlustur.width(10);
		kitabiOlustur<<setiosflags(ios::right);
		kitabiOlustur<<fiati;
		
		kitabiOlustur.width(10);
		kitabiOlustur<<setiosflags(ios::right);
		kitabiOlustur<<adedi<<endl;

		kitabiOlustur.close();
	}
	void kitabiOlustur();

};
void dosya::kitabiOlustur()
{
	char temp[250];

	olustur();
	strcpy(temp,adi);
	strcat(temp,".txt");

	ofstream kitabiOlustur(temp);
	kitabiOlustur<<"Kitap Adi   : "<<adi<<endl;
	kitabiOlustur<<"Yazari      : "<<yazari<<endl;
	kitabiOlustur<<"Kitap ID    : "<<id<<endl;
	kitabiOlustur<<"Fiati       : "<<fiati<<endl;
	kitabiOlustur<<"Adeti       : "<<adedi<<endl;
	kitabiOlustur.close();
}


class Roman:public dosya
{
public:
	void dosyayaYaz()
	{
		fstream streamRoman("Roman.txt",ios::out|ios::app);
		streamRoman<<endl;
		streamRoman<<"Kitap Adi   : "<<adi<<endl;
		streamRoman<<"Yazari      : "<<yazari<<endl;
		streamRoman<<"Kitap ID    : "<<id<<endl;
		streamRoman<<"Fiati       : "<<fiati<<endl;
		streamRoman<<"Adeti       : "<<adedi<<endl;
		streamRoman.close();
	}
};


class Hikaye:public dosya
{
public:
	void dosyayaYaz()
	{
		fstream streamHikaye("Hikaye.txt",ios::out|ios::app);
		streamHikaye<<endl;
		streamHikaye<<"Kitap Adi   : "<<adi<<endl;
		streamHikaye<<"Yazari      : "<<yazari<<endl;
		streamHikaye<<"Kitap ID    : "<<id<<endl;
		streamHikaye<<"Fiati       : "<<fiati<<endl;
		streamHikaye<<"Adeti       : "<<adedi<<endl;
		streamHikaye.close();
	}
};

class Biyografi:public dosya
{
public:
	void dosyayaYaz()
	{
		fstream streamBiyografi("Biyografi.txt",ios::out|ios::app);
		streamBiyografi<<endl;
		streamBiyografi<<"Kitap Adi   : "<<adi<<endl;
		streamBiyografi<<"Yazari      : "<<yazari<<endl;
		streamBiyografi<<"Kitap ID    : "<<id<<endl;
		streamBiyografi<<"Fiati       : "<<fiati<<endl;
		streamBiyografi<<"Adeti       : "<<adedi<<endl;
		streamBiyografi.close();
	}
};

class Diger:public dosya
{
public:
	void dosyayaYaz()
	{
		fstream streamDiger("Diger.txt",ios::out|ios::app);
		streamDiger<<endl;
		streamDiger<<"Kitap Adi   : "<<adi<<endl;
		streamDiger<<"Yazari      : "<<yazari<<endl;
		streamDiger<<"Kitap ID    : "<<id<<endl;
		streamDiger<<"Fiati       : "<<fiati<<endl;
		streamDiger<<"Adeti       : "<<adedi<<endl;
		streamDiger.close();
	}
};                                             




class KitapKaydetme
{
public:
	KitapKaydetme();
};

KitapKaydetme::KitapKaydetme()
{
	Roman roman;
	Hikaye hikaye;
	Biyografi biyografi;
	Diger diger;
	int tercih;

	while(1)
	{
		ekraniTemizle();
		cout<<"\n\n * * * * * * * * * * * * KITAP KAYDET * * * * * * * * * * * * ";
		cout<<"\n1.Roman Turu \n2.Hikaye Turu \n3.Biyografi Turu \n4.Diger Turler \n5.Geri \n";
		cout<<"\nLutfen Tercihinizi Giriniz:";
		cin>>tercih;

		if(tercih==1)
		{
			roman.kitabiOlustur();
			roman.dosyayaYaz();
			roman.hepsiListesineYaz();
		}else if(tercih==2)
		{
			hikaye.kitabiOlustur();
			hikaye.dosyayaYaz();
			hikaye.hepsiListesineYaz();
		}else if(tercih==3)
		{
			biyografi.kitabiOlustur();
			biyografi.dosyayaYaz();
			biyografi.hepsiListesineYaz();
		}else if(tercih==4)
		{
			diger.kitabiOlustur();
			diger.dosyayaYaz();
			diger.hepsiListesineYaz();
		}else if(tercih==5)
		{
			break;
		}
	}
}        


// ************************************** GOSTERIM SINIFI ******************************************************************************* //

class GosterimYazar
{
public:
	GosterimYazar();
};


GosterimYazar::GosterimYazar()
{	
	char ch;

	ekraniTemizle();

	cout<<endl;
	cout.width(20);
	cout<<setiosflags(ios::left);
	cout<<"YAZAR ADI";

	cout.width(15);
	cout<<setiosflags(ios::left);
	cout<<"DOGUM YERI";

	cout.width(12);
	cout<<setiosflags(ios::right);
	cout<<"DOGUM YILI";

	cout.width(20);
	cout<<setiosflags(ios::right);
	cout<<"POPULARITESI"<<endl<<endl;

	fstream stream("Yazarlar.txt",ios::in);
	stream.seekg(0);
	while(stream)
	{
		stream.get(ch);
		cout<<ch;
	}
	stream.close();
	getch();		
}

class Gosterim
{
public:
	Gosterim();
};


Gosterim::Gosterim()
{
	int tercih;
	char id[10],ch;
	while(1){
		ekraniTemizle();
		cout<<"\n\n * * * * * * * * * * * * GOSTER * * * * * * * * * * * * \n\n";
		cout<<"1.Hepsini"<<endl;
		cout<<"2.Roman"<<endl;
		cout<<"3.Hikaye"<<endl;
		cout<<"4.Biyografi"<<endl;
		cout<<"5.Diger"<<endl;
		cout<<"6.Geri"<<endl;

		cout<<"\nLutfen Tercihinizi Giriniz:" ;
		cin>>tercih;

		if(tercih==1){
			ekraniTemizle();

			cout<<endl;
			cout.width(20);
			cout<<setiosflags(ios::left);
			cout<<"KITAP ADI";

			cout.width(18);
			cout<<setiosflags(ios::left);
			cout<<"YAZARI";

			cout.width(9);
			cout<<setiosflags(ios::right);
			cout<<"ID";

			cout.width(10);
			cout<<setiosflags(ios::right);
			cout<<"FIATI";

			
			cout.width(10);
			cout<<setiosflags(ios::right);
			cout<<"ADEDI"<<endl<<endl;

			fstream stream("Kitaplar.txt",ios::in);
			stream.seekg(0);
			while(stream)
			{
				stream.get(ch);
				cout<<ch;
			}
			stream.close();
			getch();
		}else if(tercih==2){
			ekraniTemizle();
			fstream kitabiOlustur("Roman.txt",ios::in);
			kitabiOlustur.seekg(0);
			cout<<"\n";
			while(kitabiOlustur)
			{
				kitabiOlustur.get(ch);
				cout<<ch;
			}
			kitabiOlustur.close();
			getch();
		}else if(tercih==3){
			ekraniTemizle();
			fstream kitabiOlustur("Hikaye.txt",ios::in);
			kitabiOlustur.seekg(0);
			cout<<"\n";
			while(kitabiOlustur)
			{
				kitabiOlustur.get(ch);
				cout<<ch;
			}
			kitabiOlustur.close();
			getch();
		}else if(tercih==4){
			ekraniTemizle();
			fstream kitabiOlustur("Biyografi.txt",ios::in);
			kitabiOlustur.seekg(0);
			cout<<"\n";
			while(kitabiOlustur)
			{
				kitabiOlustur.get(ch);
				cout<<ch;
			}
			kitabiOlustur.close();
			getch();
		}else if(tercih==5){
			ekraniTemizle();
			fstream kitabiOlustur("Diger.txt",ios::in);
			kitabiOlustur.seekg(0);
			cout<<"\n";
			while(kitabiOlustur)
			{
				kitabiOlustur.get(ch);
				cout<<ch;
			}
			kitabiOlustur.close();
			getch();
		}else if(tercih==6)
			break;
	}
}

// ************************************** ARAMA SINIFI ******************************************************************************* //

class Arama
{
public:
	Arama();
};



Arama::Arama()
{
	int tercih;
	char temp[250];
	char ad[250];
	char karakter;
	while(1)
	{
		ekraniTemizle();
		cout<<"\n\n * * * * * * * * * * * * ARAMA * * * * * * * * * * * * \n\n";
		cout<<"1.Yazar Ara";
		cout<<"\n2.Kitap Ara";
		cout<<"\n3.Geri\n";
		cout<<"\nLutfen Tercihinizi Giriniz:";
		cin>>tercih;
		
		if(tercih==1){
			int eslesen=0;
			
			cout<<"Yazar Adi Giriniz:";
			cin.ignore();
			cin.getline(ad,250);
			cout<<endl;

			// Yazarlar.txt dosyasini okumak icin ac
			ifstream fileInput;
			fileInput.open("Yazarlar.txt");

			// dosya icinde arama yap
			string line;
			unsigned int curLine = 0;
			while(getline(fileInput, line)) {
				curLine++; 	
				myUpperCase(ad);// girilen metni buyuk harfe donustur
				
				if (line.find(ad, 0) != string::npos)  { // eger eslesen kayit bulunursa
					eslesen++;
					cout<<line<<endl;
				}
			}

			// Sonucta kac tane kayit eslesti ekrana yaz
			if(eslesen>0){
				cout << "\n" << eslesen <<" adet kayit bulundu!\n" << endl;
			}else{
				cout << "\n"  << "Hic kayit bulunamadi!\n" << endl;
			}

			getch();
		}else if(tercih==2){
			int eslesen=0;
			
			cout<<"Kitap Adi yada Yazarini Giriniz:";
			cin.ignore();
			cin.getline(ad,250);
			cout<<endl;

			// Yazarlar.txt dosaysini okumak icin ac
			ifstream fileInput;
			fileInput.open("Kitaplar.txt");

			// dosya icinde arama yap
			string line;
			unsigned int curLine = 0;
			while(getline(fileInput, line)) {
				curLine++; 	
				myUpperCase(ad);// girilen metni buyuk harfe donustur

				if (line.find(ad, 0) != string::npos) { // eger eslesen kayit bulunursa
					eslesen++;
					cout<<line<<endl;
				}
			}

			// Sonucta kac tane kayit eslesti ekrana yaz
			if(eslesen>0){
				cout << "\n" << eslesen <<" adet kayit bulundu!\n" << endl;
			}else{
				cout << "\n"  << "Hic kayit bulunamadi!\n" << endl;
			}
			
			getch();
		}else
			break;
	}
}


// ************************************** SATIS SINIFI ******************************************************************************* //

class Satis
{
public:
	Satis();
};



Satis::Satis()
{
	int tercih;
	char temp[250];
	char ad[250];
	char karakter;
	while(1)
	{
		ekraniTemizle();
		cout<<"\n\n * * * * * * * * * * * * SATIS * * * * * * * * * * * * \n\n";
		cout<<"\n1.Yeni Satis";
		cout<<"\n2.Geri \n";
		cout<<"\nLutfen Tercihinizi Giriniz:";
		cin>>tercih;
		
		if(tercih==1){
			int eslesen=0;
			
			cout<<"Kitap ID'si Giriniz:";
			cin.ignore();
			cin.getline(ad,250);
			cout<<endl;

			// Kitaplar.txt dosyasini okumak icin ac
			ifstream fileInput;
			fileInput.open("Kitaplar.txt");

			// dosya icinde arama yap
			string line;
			unsigned int curLine = 0;
			while(getline(fileInput, line)) {
				curLine++; 	
				myUpperCase(ad);// girilen metni buyuk harfe donustur

				string word(ad);
				if (contains_word(line, word)) { // eger eslesen kayit bulunursa
					eslesen++;
					cout<<line<<endl;

					break;
				}
			}

			// Sonucta kac tane kayit eslesti ekrana yaz
			if(eslesen>0){
				cout << "\n" << eslesen <<" adet satis yapildi!\n" << endl;
			}else{
				cout << "\n"  << "Hic kayit bulunamadi!\n" << endl;
			}

			getch();
		}else if(tercih==2){
			int eslesen=0;
			
			cout<<"Kitap Adi yada Yazarini Giriniz:";
			cin.ignore();
			cin.getline(ad,250);
			cout<<endl;

			// Yazarlar.txt dosaysini okumak icin ac
			ifstream fileInput;
			fileInput.open("Kitaplar.txt");

			// dosya icinde arama yap
			string line;
			unsigned int curLine = 0;
			while(getline(fileInput, line)) {
				curLine++; 	
				myUpperCase(ad);// girilen metni buyuk harfe donustur

				if (line.find(ad, 0) != string::npos) { // eger eslesen kayit bulunursa
					eslesen++;
					cout<<line<<endl;
				}
			}

			// Sonucta kac tane kayit eslesti ekrana yaz
			if(eslesen>0){
				cout << "\n" << eslesen <<" adet kayit bulundu!\n" << endl;
			}else{
				cout << "\n"  << "Hic kayit bulunamadi!\n" << endl;
			}
			
			getch();
		}else
			break;
	}
}


// ************************************** GIRIS SINIFI ******************************************************************************* //

class Giris
{
public:
	Giris();
};

Giris::Giris()
{
	int tercih;

	while(1)
	{
		ekraniTemizle();
		cout<<"\n\n * * * * * * * * * * * * ISLEM SECIN * * * * * * * * * * * * \n\n";
		cout<<"1.Yazar Kaydet\n";
		cout<<"2.Kitap Kaydet\n";
		cout<<"3.Yazarlari Listele\n";
		cout<<"4.Kitaplari Listele\n";
		cout<<"5.Arama\n";
		cout<<"6.Satis Yap\n";
		cout<<"7.Cikis\n";
		cout<<"\nLutfen Tercihinizi Giriniz:";
		cin>>tercih;

		if(tercih==1) YazarKaydetme y;
		else if(tercih==2) KitapKaydetme k;		
		else if(tercih==3) GosterimYazar gy;
		else if(tercih==4) Gosterim g;
		else if(tercih==5) Arama a; 
		else if(tercih==6) Satis s;
		else break;
	}
}  

int main()
{
	Giris end;
	return 0;
}
